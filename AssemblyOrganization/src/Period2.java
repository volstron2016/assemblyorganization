
public class Period2 implements Comparable {

/**
 * Period2 is used by MyFileIO to identify how to use column headers as elements in Array	
 */
	String CourseTitle;
	String Teacher;
	String TermCode;
	int RoomNumber;
	int TotalStudents;
	
	public Period2(String line){
		String[] data = line.split("\\t");
		int i = 0;
		
		CourseTitle = data[i]; i++;
		Teacher = data[i]; i++;
		TermCode = data[i]; i++;
		
		if(data[i].toLowerCase().equals("33a")){
			String str = data[i].toLowerCase().substring(0,2);
			RoomNumber = Integer.parseInt(str);
			
		}
		
		else if(data[i].toLowerCase().equals("42a")){
			RoomNumber=301;
			
		}
		else if(data[i].toLowerCase().equals("coun")){
			RoomNumber=302;
			
		}
		else if(data[i].toLowerCase().equals("gh")){
			RoomNumber=303;
			
		}
		else if(data[i].toLowerCase().equals("gym")){
			RoomNumber=304;
			
		}
		else if(data[i].toLowerCase().equals("main")){
			RoomNumber=305;
			
		}
		else if(data[i].toLowerCase().equals("mc")){
			RoomNumber=306;
			
		}
		else if(data[i].toLowerCase().equals("out")){
			RoomNumber=307;
			
		}
		else{
			RoomNumber = Integer.parseInt(data[i]);
		}
		i++;
		
		
		//RoomNumber = Integer.parseInt(data[i]); i++;
		TotalStudents = Integer.parseInt(data[i]); i++;
	}
	
	public String toString() {
		return "Period 2 [" +
				"Course Title=" + CourseTitle +
				", Teacher=" + Teacher + 
				", Term Code=" + TermCode +
				", Room Number=" + RoomNumber +
				", Total Students=" + TotalStudents +  "]";
	}
	
	public String getTeacherName(){
		return Teacher;
	}

	public String getTermCode() {
		return TermCode;
	}


	public int getTotalStudents() {
		return TotalStudents;
	}
	
	public int getRoomNumber(){
		return RoomNumber;
	}


	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	

}

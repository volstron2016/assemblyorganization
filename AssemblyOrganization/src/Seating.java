import java.awt.Color;
import java.awt.Font;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.FileNotFoundException;
import java.util.ArrayList;


import javax.swing.JPanel;

/**
 * Generates a JPanel which will create ovals for shapes in the GUI. Class calls Auditorium.java for creating seats
 * based upon amount of students. This class creates all the colors that are displayed on the gui and draw the room
 * numbers on top of the ovals generated. 
 */

public class Seating extends JPanel{
	private int x1=0;
	private int y1=0;
	private int width = 12;
	private int height = 12;
	private ArrayList<Integer> roomNumberArray = new ArrayList<Integer>();
	
	static int numToAdd = 1;
	
	int numX;
	int numY;
	String termCode;
	int classToPrioritize;
	
	public Seating(int numX, int numY, String termCode, int classToPrioritize){
		super();
		this.numX = numX;
		this.numY = numY;
		this.termCode = termCode;
		this.classToPrioritize = classToPrioritize;
	}
	
	/*Gets ArrayList from Auditorium.java by calling its shapesToCreate method which will return
	 * an Integer Array with all the roomNumbers based on amount of people in that room. i.e. class full of
	 * 30 students including the teacher in room 27 will generate an amount of 30 27's in the ArrayList  
	 */
	public  ArrayList<Integer> generateArrayList(){
		ArrayList<Integer> shapesToCreate = null;
		try {
			shapesToCreate = Auditorium.shapesToCreate(numX, numY, termCode, classToPrioritize);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return shapesToCreate;
	}

	/*
	 * Generates ArrayList of  47 colors and colors are hard coded so nice looking colors are displayed instead of random
	 * colors which were too close together 
	 */
	public static ArrayList<Color> createColorArray(){
		ArrayList<Color> colors = new ArrayList<Color>();

		
		colors.add(new Color(204, 0, 0));
		colors.add(new Color(204, 102, 0));
		colors.add(new Color(204, 204, 0));
		colors.add(new Color(102, 204, 0));
		colors.add(new Color(0, 204, 0));
		colors.add(new Color(204, 102, 0));
		colors.add(new Color(0, 204, 204));
		colors.add(new Color(0, 102, 0));
		colors.add(new Color(0, 0, 204));
		colors.add(new Color(102, 0, 204));
		colors.add(new Color(204, 0, 204));
		colors.add(new Color(204, 0, 102));
		colors.add(new Color(51, 0, 102));
		colors.add(new Color(255, 0, 0));
		colors.add(new Color(255, 128, 0));
		colors.add(new Color(255, 255, 0));
		colors.add(new Color(128, 255, 0));
		colors.add(new Color(0, 255, 0));
		colors.add(new Color(0, 255, 128));
		colors.add(new Color(0, 255, 255));
		colors.add(new Color(0, 255, 0));
		colors.add(new Color(0, 128, 255));
		colors.add(new Color(0, 0, 255));
		colors.add(new Color(127, 0, 255));
		colors.add(new Color(0, 0, 255));
		colors.add(new Color(255, 0, 255));
		colors.add(new Color(255, 0, 127));
		colors.add(new Color(102, 0, 102));
		colors.add(new Color(255, 153, 51));
		colors.add(new Color(255, 255, 51));
		colors.add(new Color(153, 255, 51));
		colors.add(new Color(51, 255, 51));
		colors.add(new Color(51, 255, 153));
		colors.add(new Color(51, 255, 255));
		colors.add(new Color(51, 153, 255));
		colors.add(new Color(51, 51, 255));
		colors.add(new Color(153, 51, 255));
		colors.add(new Color(255, 51, 255));
		colors.add(new Color(255, 51, 153));
		colors.add(new Color(160, 160, 160));
		colors.add(new Color(255, 102, 102));
		colors.add(new Color(255, 178, 102));
		colors.add(new Color(255, 255, 102));
		colors.add(new Color(178, 255, 102));
		colors.add(new Color(102, 255, 102));
		colors.add(new Color(102, 255, 178));
		colors.add(new Color(102, 255, 255));

		return colors;
	}
	
	/*
	 * Uses ArrayList created from generated ArrayList and creates a new ArrayList that has all roomNumbers replaced 
	 * starting at 1 and going through. It does not replaces 0's or 500's as 0's indicate new line and 500's are empty 
	 * seats
	 * 
	 *  i.e generatedArrayList = [1,1,1,1,1,2,2,2,2,2,2,0,7,7,7,7,7,7,7,9,9,9,9,9,9,500...]
	 *      outputedArrayList  = [1,1,1,1,1,2,2,2,2,2,2,0,3,3,3,3,3,3,3,4,4,4,4,4,4,500...] 
	 */
	public static ArrayList<Integer> numbered(ArrayList<Integer> original) {
		ArrayList<Integer> x = new ArrayList<Integer>();
		int c=1;

		int temp = original.get(0);
		
		x.add(numToAdd);
		 while (c < original.size()){
			if(original.get(c) == 0){
				x.add(0);
				c++;
			}
			else if(original.get(c) == 500){
				x.add(500);
				c++;
			}
			else if(original.get(c) == temp){
				x.add(numToAdd);
				c++;
			}
			else{
				temp = original.get(c);
				numToAdd++;
			}
			
		}
		return x;
	}
	
	/*
	 * Lots of zeros in beginning of the generatedArray so this method removes them
	 * i.e generatedArrayList = [0,0,0,0,0,1,1,1,1,1,1,1,1,2...]
	 *     outputedArrayList  = [1,1,1,1,1,1,1,1,2...]
	 */
	
	public static Color get(float x) {
		float r = 0.0f;
		float g = 0.0f;
		float b = 1.0f;
		if (x >= 0.0f && x < 0.2f) {
			x = x / 0.2f;
			r = 0.0f;
			g = x;
			b = 1.0f;
		} else if (x >= 0.2f && x < 0.4f) {
			x = (x - 0.2f) / 0.2f;
			r = 0.0f;
			g = 1.0f;
			b = 1.0f - x;
		} else if (x >= 0.4f && x < 0.6f) {
			x = (x - 0.4f) / 0.2f;
			r = x;
			g = 1.0f;
			b = 0.0f;
		} else if (x >= 0.6f && x < 0.8f) {
			x = (x - 0.6f) / 0.2f;
			r = 1.0f;
			g = 1.0f - x;
			b = 0.0f;
		} else if (x >= 0.8f && x <= 1.0f) {
			x = (x - 0.8f) / 0.2f;
			r = 1.0f;
			g = 0.0f;
			b = x;
		}
		return new Color(r, g, b);
	}

	public static ArrayList<Integer> removeExtras(ArrayList<Integer> example) {

		for (int i = 0; i < example.size() - 1; i++) {
			int w = example.get(i);
			for (int x = example.lastIndexOf(w) - example.indexOf(w); x > 0; x--) {
				example.remove(i);
			}
		}
		example.remove(example.size()-1);
		return example;
	}
	
	
	/*
	 * This paints all the data on to a JPanel
	 */
	@Override
      protected void paintComponent(Graphics g){
		super.paintComponent(g);
		super.setBackground(Color.WHITE);
		Graphics2D g2d = (Graphics2D) g.create();
		String msg = null;
		
		//Generate ArrayLists
		ArrayList<Integer> dataList = generateArrayList();
		ArrayList<Integer> roomNumberArray = Seating.numbered(dataList);
		ArrayList<Color> colors = createColorArray();

		x1 = 0;
		y1 = 0;
		String emptySeats = "E";  //500

		//create oval and add roomNumber on top of it 
		for(int i = 0; i < roomNumberArray.size(); i++){
			if(roomNumberArray.get(i) == 0){
				y1 = y1+12;
				x1=0;
			}
			else if(roomNumberArray.get(i) == 500){
				g2d.setColor(Color.GRAY);
				g2d.fillOval(x1, y1, width, height);
			
				g2d.setColor(Color.BLACK);
				g.drawString(emptySeats, x1+2, y1+12);
				x1 = x1+14;
			}
			else{
				int Number = dataList.get(i);
				
				if((Number != 0 || Number != 500) && i < roomNumberArray.size()){
					String text1 = "" + Number;
					
					g2d.setColor(colors.get(roomNumberArray.get(i)%47));
					g2d.fillOval(x1, y1, width, height);
					
		
					g2d.setFont(new Font("TimesRoman", Font.BOLD, 12));
					g.drawString(text1, x1+2, y1+12);
					
					x1 = x1+14;
				}

			}
		}

		g2d.dispose();
		
      }
	



		
}


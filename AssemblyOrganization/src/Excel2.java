import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * This class is where all the work with excel occurs. Its imported through JFileChooser and then all data not needed
 * is removed and an output.txt is created from where the program checks everything. Uses Apache POI external JAR to access
 * data from the excel sheet
 */


public class Excel2  {
	
	public static String temp = "";
	
	public static void deleteColumn(Sheet sheet, int columnToDelete) {
		int maxColumn = 0;
		for (int r = 0; r < sheet.getLastRowNum() + 1; r++) {
			Row row = sheet.getRow(r);

			// if no row exists here; then nothing to do; next!
			if (row == null)
				continue;

			// if the row doesn't have this many columns then we are good; next!
			int lastColumn = row.getLastCellNum();
			if (lastColumn > maxColumn)
				maxColumn = lastColumn;

			if (lastColumn < columnToDelete)
				continue;

			for (int x = columnToDelete + 1; x < lastColumn + 1; x++) {
				Cell oldCell = row.getCell(x - 1);
				if (oldCell != null)
					row.removeCell(oldCell);

				Cell nextCell = row.getCell(x);
				if (nextCell != null) {
					Cell newCell = row
							.createCell(x - 1, nextCell.getCellType());
					cloneCell(newCell, nextCell);
				}
			}
		}
	}

	private static void cloneCell(Cell cNew, Cell cOld) {

		cNew.setCellComment(cOld.getCellComment());
		cNew.setCellStyle(cOld.getCellStyle());

		switch (cNew.getCellType()) {
		case Cell.CELL_TYPE_BOOLEAN: {
			cNew.setCellValue(cOld.getBooleanCellValue());
			break;
		}
		case Cell.CELL_TYPE_NUMERIC: {
			cNew.setCellValue(cOld.getNumericCellValue());
			break;
		}
		case Cell.CELL_TYPE_STRING: {
			cNew.setCellValue(cOld.getStringCellValue());
			break;
		}
		case Cell.CELL_TYPE_ERROR: {
			cNew.setCellValue(cOld.getErrorCellValue());
			break;
		}
		case Cell.CELL_TYPE_FORMULA: {
			cNew.setCellFormula(cOld.getCellFormula());
			break;
		}
		}

	}
	
	public static void removeFirstLine(String fileName) throws IOException {  
	    RandomAccessFile raf = new RandomAccessFile(fileName, "rw");          
	     //Initial write position                                             
	    long writePosition = raf.getFilePointer();                            
	    raf.readLine();                                                       
	    // Shift the next lines upwards.                                      
	    long readPosition = raf.getFilePointer();                             

	    byte[] buff = new byte[1024];                                         
	    int n;                                                                
	    while (-1 != (n = raf.read(buff))) {                                  
	        raf.seek(writePosition);                                          
	        raf.write(buff, 0, n);                                            
	        readPosition += n;                                                
	        writePosition += n;                                               
	        raf.seek(readPosition);                                           
	    }                                                                     
	    raf.setLength(writePosition);                                         
	    raf.close();                                                          
	}  

	public static void ExcelFileChooser() throws IOException {
		JFileChooser filechooser = new JFileChooser();
		int returnValue = filechooser.showOpenDialog(null);
		PrintWriter f0 = new PrintWriter(new FileWriter("output.txt"));
		
		JOptionPane lineSelector = new JOptionPane();
		String number = lineSelector.showInputDialog("Last Line Number with data in it");
		int lineNumber = Integer.parseInt(number);
		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			try {
				Workbook workbook = new HSSFWorkbook(new FileInputStream(filechooser.getSelectedFile()));
				Sheet sheet = workbook.getSheetAt(0);

				// Deletes unnecessary text/data in excel file in the beginning
				// and end of spreadsheet
				int i = 0;
				Cell cell1 = null;

				while (i < 17) {
					try {
						cell1 = sheet.getRow(i).getCell(0);
					} catch (NullPointerException e) {
						i++;
					}
					if (cell1 != null) {
						cell1.setCellValue("");
						i++;
					} else {
						i++;
					}

				}

				int k = 0;
				Cell cell2 = null;
				while (k < lineNumber) {
					try {
						cell2 = sheet.getRow(k).getCell(0);
					} catch (NullPointerException e) {
						k++;
					}
					if (cell2 != null) {
						cell2.setCellValue("");
						k++;
					} else {
						i++;
					}
				}

				deleteColumn(sheet, 0);
				deleteColumn(sheet, 0);
				deleteColumn(sheet, 1);
				deleteColumn(sheet, 1);
				deleteColumn(sheet, 4);
				deleteColumn(sheet, 4);
				deleteColumn(sheet, 5);
				deleteColumn(sheet, 5);

				
				for (Iterator<Row> rit = sheet.rowIterator(); rit.hasNext();) {
					Row row = rit.next();

					for (Iterator<Cell> cit = row.cellIterator(); cit.hasNext();) {
						Cell cell = cit.next();
						cell.setCellType(Cell.CELL_TYPE_STRING);

						f0.print(cell.getStringCellValue() + "\t");

					}
					f0.println();

				}
				
				f0.close();

				
				for(int a=0; a < 15; a++)
					removeFirstLine("output.txt");
				
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		}

		
	}

}

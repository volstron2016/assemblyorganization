import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 * 
 * The JFrame where all the JPanels are added. This class basically takes everything and puts it onto a JFrame and
 * adds buttons and uses the results to generate the desired view of the Auditorium
 *
 */

public class seatingTester {
	
	private static String s = "S1";
	private static final String IMG_PATH = "volstron_logo.png";
	
	public static ArrayList<Period2> createPeriod2Array() throws FileNotFoundException{
		ArrayList<Period2> period2 = MyFileIO.getList();
		return period2;
}
	
	public static void main(String[] args) throws IOException {
		
		
		final JFrame f = new JFrame("Auditorium");
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.setPreferredSize(new Dimension(1000, 800));
		
		f.setLayout(new BorderLayout());
		
		JLabel stageLabel = new JLabel();
		stageLabel.setPreferredSize(new Dimension(50, 50));
		stageLabel.setText("                                                                                                           STAGE");
		
		
		JPanel firstPanel = new JPanel();
		JPanel buttonPanel = new JPanel();
		JPanel messagePanel = new JPanel(new GridLayout(2,2));
		
		JLabel welcomeMessage = new JLabel();
		JButton nextButton = new JButton();
		
		buttonPanel.add(nextButton);
		
		
		BufferedImage img = ImageIO.read(new File(IMG_PATH));
		ImageIcon icon = new ImageIcon(img);
		JLabel label = new JLabel(icon);
		
		messagePanel.add(welcomeMessage);
		messagePanel.add(label);
		messagePanel.add(buttonPanel);
		
		JTextArea tutorial = new JTextArea();
		tutorial.setEditable(false);
		String tut = "To get the data: \n Go to OASIS Database. \n Select class period that assembly will be in. \n Click Multisort. \n Select all Checkboxes. \n Save as a Printer Friendly file. \n Copy all data into an Excel sheet, and look at the last row that data is contained in. \n Save as an xls file. (Excel 97-2003 Workbook) \n \n Program: \n Launch the program from Mrs. Orrence�s HandIn file. \n  Select the Start button. \n Select created Excel file with all data. \n Type in the last row number into the box. \n Select semester from drop down menu. \n DONE!";
		
		tutorial.setText(tut);
		
		welcomeMessage.setText("         Welcome to the Volstron Assembly Organization Program");
		nextButton.setText("Start");
		
		
		//Creates Welcome Screen Panel with Tutorial/Steps on what to do to run program
		firstPanel.add(messagePanel);
		firstPanel.add(tutorial);

		//Adds the Panel to the Frame
		f.add(firstPanel);
		
	
		
		//Adds listener for the button on welcome Screen
		nextButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				f.getContentPane().removeAll();
				f.getContentPane().validate();
				f.repaint();
				try {
					//Launch excel importer
					Excel2.ExcelFileChooser();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				ArrayList<Period2> period2 = new ArrayList<Period2>();
				
				try {
					period2 = MyFileIO.getList();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Object[] possibilities = {"S1", "S2"};
				s = (String)JOptionPane.showInputDialog(f, "Please select the Semester below: ", "Customized Dialog", JOptionPane.PLAIN_MESSAGE, null, possibilities, "S1");
				f.validate();
				f.repaint();
				
				ArrayList<String> temp = new ArrayList<String>();
				temp = MyFileIO.getArrayOfTeacherNamesAndRoomNumber(period2, s);
				
				ArrayList<String> teacherNames = new ArrayList<String>();
				teacherNames = MyFileIO.removeExtras(temp);
				
				teacherNames.add("None");
				
				Object[] roomPossibilities = new Object[teacherNames.size()];
				
				for(int i=0; i<teacherNames.size(); i++){
					roomPossibilities[i] = teacherNames.get(i); 
				}
				
				teacherNames.remove("None");
				
				 Object p = (String)JOptionPane.showInputDialog(f, "Please Select the Room you would like to Prioritize: ", "Customized Dialog", JOptionPane.PLAIN_MESSAGE, null, roomPossibilities, roomPossibilities[0]);
				 
				 int answer = 0;
				 
				 for(int i=0; i<roomPossibilities.length; i++){
					 if(roomPossibilities[i].equals("None")){
						 answer = -1;
						 break;
					 }
					 else if(roomPossibilities[i].equals(p)){
						 answer = i;
						 break;
					 }
				 }
				 
				//int prioritizeNumber = Integer.parseInt((p.replaceAll("\\D+", "")));
				
				
				f.validate();
				f.repaint();
				
				Font labelFont = new Font("Times New", Font.PLAIN, 9);
				
				
				JTextArea rowLabel1 = new JTextArea("A\nB\nC\nD\nE\nF\nG\nH\nJ\nK\nL\nM\nN\nO\nAA\nBB\nCC\nDD\nEE\nFF\nGG\nHH");
				rowLabel1.setFont(labelFont);
				//rowLabel.setText("A\nB\nC\nD\nE\nF\nG\nH\nI\nJ\nK\nL\nM\nN\nO\nP\nQ\nR\nS\nT\nU\nV\nW\nX\nY\nZ");
				JTextArea rowLabel2 = new JTextArea("A\nB\nC\nD\nE\nF\nG\nH\nJ\nK\nL\nM\nN\nO\n1\n2\n3\n4\n5\n6\n7");
				rowLabel2.setFont(labelFont);
				JTextArea rowLabel3 = new JTextArea("A\nB\nC\nD\nE\nF\nG\nH\nJ\nK\nL\nM\nN\nO\nAA\nBB\nCC\nDD\nEE\nFF\nGG\nHH");
				rowLabel3.setFont(labelFont);
				JTextArea rowLabel4 = new JTextArea("A\nB\nC\nD\nE\nF\nG\nH\nJ\nK\nL\nM\nN\nO\n1\n2\n3\n4\n5\n6\n7");
				rowLabel4.setFont(labelFont);
				
				JPanel rowLabelPanel1 = new JPanel();
				JPanel rowLabelPanel2 = new JPanel();
				JPanel rowLabelPanel3 = new JPanel();
				JPanel rowLabelPanel4 = new JPanel();
				
				rowLabelPanel1.add(rowLabel1);
				rowLabelPanel2.add(rowLabel2);
				rowLabelPanel3.add(rowLabel3);
				rowLabelPanel4.add(rowLabel4);
				
				JPanel master = new JPanel();
				master.setLayout(new GridLayout(2,5));

				
				Seating a = new Seating(0, 0, s, answer);
				Seating b = new Seating(1, 0, s, -1);
				Seating c = new Seating(2, 0, s, -1);
				Seating d = new Seating(0, 1, s, -1);
				Seating l = new Seating(1, 1, s, -1);
				Seating h = new Seating(2, 1, s, -1);
				
				master.add(a);
				master.add(rowLabelPanel1);
				master.add(b);
				master.add(rowLabelPanel2);
				master.add(c);
				master.add(d);
				master.add(rowLabelPanel3);
				master.add(l);
				master.add(rowLabelPanel4);
				master.add(h);
				
				f.getContentPane().add(master);
				f.getContentPane().validate();
				f.repaint();
				
				f.getContentPane().add(master, BorderLayout.CENTER);
				f.getContentPane().validate();
				f.repaint();
				
				JPanel keyPanel = new JPanel();
				keyPanel.setLayout(new BorderLayout());
				
				JButton exportButton = new JButton("Export");
				
				JTextArea keyText = new JTextArea();
				keyText.setEditable(false);
				
	
				keyText.setText(keyText.getText() + "E Empty Seat \n");
				for(int i=0; i<teacherNames.size(); i++){
					System.out.println(teacherNames.get(i));
					keyText.setText(keyText.getText() + teacherNames.get(i) + "\n");
				}
				
				
				JScrollPane scroll = new JScrollPane(keyText);
				scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
				
			
				JLabel stageLabel = new JLabel();
				stageLabel.setPreferredSize(new Dimension(50, 50));
				

				stageLabel.setText("                                                                                                                          STAGE");
				
				f.add(stageLabel, BorderLayout.NORTH);
				
				keyPanel.add(exportButton, BorderLayout.NORTH);
				keyPanel.add(scroll);

				
				exportButton.addActionListener(new ExportButton(exportButton, f));
				f.getContentPane().validate();
				f.repaint();
				
				f.add(keyPanel, BorderLayout.EAST);
				f.getContentPane().validate();
				f.repaint();
				
			}
		});
			
		
		f.pack();
		f.setVisible(true);
		
		
}
}

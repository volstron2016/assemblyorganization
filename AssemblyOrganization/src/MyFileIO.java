import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class MyFileIO {
	
	/**
	 * This program is where the txt file is read through and creates arrays for us to access throughout the program
	 */
	
    private static final String fileName = "output.txt";
	private Scanner myFile = null;

	// No-args constructor, create a new scanner for the specific file defined
	// above
	public MyFileIO() throws FileNotFoundException {
		if (myFile == null)
			myFile = new Scanner(new File(fileName));
	}

	// One-arg constructor - the name of the file to open
	public MyFileIO(String name) throws FileNotFoundException {
		if (myFile != null)
			myFile.close();
		myFile = new Scanner(new File(name));
	}

	// Return the next line from the file
	public String nextLine() {
		return myFile.nextLine().trim();
	}

	// Return true if there are no more lines in the file
	public boolean endOfFile() { // Return true is there is no more input
		return !myFile.hasNext(); // hasNext() returns true if there is more
									// input, so I negate it
	}

	// Close the file that we opened in the constructor.
	public void closeFile() {
		myFile.close();
	}
	
	public static int getTotalStudents(ArrayList<Period2> courses, String termCode){
		int sum = 0;
		for(int i=0; i < courses.size(); i++){
			if(courses.get(i).getTermCode().equals(termCode)){
				sum = sum + courses.get(i).getTotalStudents();
			}
		}
		return sum;
	}
	
	public static ArrayList<Integer> getArrayOfRoomNumbers(ArrayList<Period2> courses, String termCode){
		ArrayList<Integer> temp = new ArrayList<Integer>();
		for(int i=0; i<courses.size(); i++){
			if(courses.get(i).getTermCode().equals(termCode))
				temp.add(courses.get(i).getRoomNumber());
		}
		return temp;
	}
	
	public static ArrayList<String> getArrayOfTeacherNames(ArrayList<Period2> courses, String termCode){
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0; i<courses.size(); i++){
			if(courses.get(i).getTermCode().equals(termCode))
				temp.add(courses.get(i).getTeacherName());
		}
		return temp;
	}

	public static ArrayList<String> getArrayOfTeacherNamesAndRoomNumber(ArrayList<Period2> courses, String termCode){
		ArrayList<String> temp = new ArrayList<String>();
		for(int i=0; i<courses.size(); i++){
			if(courses.get(i).getTermCode().equals(termCode))
				temp.add(courses.get(i).getRoomNumber() + " " + courses.get(i).getTeacherName());
		}
		return temp;
	}
	
	
	public static ArrayList<String> removeExtras(ArrayList<String> example) {

		for (int i = 0; i < example.size() - 1; i++) {
			String w = example.get(i);
			for (int x = example.lastIndexOf(w) - example.indexOf(w); x > 0; x--) {
				example.remove(i);
			}
		}
		example.remove(example.size()-1);
		return example;
	}
	
	
	
	public static ArrayList<Period2> getList() throws FileNotFoundException {
		// Open the text file
		MyFileIO file = new MyFileIO();
		
		// Create an ArrayList to hold all the period 2 data
		ArrayList<Period2> period2 = new ArrayList<Period2>();
		
		// First line in the file is a "header" line that has all the column headings in it. Skip over it.
		String headerLine = file.nextLine();
		
		// Loop through the file reading every line
		while(!file.endOfFile()) {
			String line = file.nextLine();
			
			// The constructor for the Period2 class takes a line of data (columns are 
			// separated by tabs) and fills in all the Period2 fields.
			Period2 course = new Period2(line);
			
			// Add this new Course object to the ArrayList.
			period2.add(course);
		}
		file.closeFile();
		Collections.sort(period2, new TermCodeSorter());
		return period2;
	}
	
}

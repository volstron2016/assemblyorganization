import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Auditorium {
	// hard coded number of seats in each row of the auditorium
	static int[][] seats = { { 7, 9, 7 }, { 8, 10, 8 }, { 8, 10, 8 },
		{ 9, 10, 9 }, { 10, 11, 10 }, { 8, 9, 10 }, { 11, 12, 11 },
		{ 12, 12, 12 }, { 12, 12, 12 }, { 13, 13, 13 }, { 14, 14, 14 },
		{ 14, 14, 8 }, { 14, 14, 8 }, { 14, 14, 8 }, { 9, 0, 15 },
		{ 8, 0, 15 }, { 13, 0, 15 }, { 13, 0, 15 }, { 12, 0, 10 },
		{ 11, 0, 10 }, { 11, 0, 5 }, { 11, 0, 0 } };
	// hard coded number of seats in each column of the auditorium
	static int[] totals = { 242, 164, 223 };

	/**
	 * Returns an ArrayList with the room number of each class printed the
	 * amount of times that there are students. There are a few important
	 * markers: "0" indicates a jump to the next row/line "500" indicates an
	 * empty seat This method may only complete one column (out of six) of the
	 * auditorium at a time. The first parameter is called firstinput, which
	 * indicates the column number that the method should focus on. The second
	 * parameter is called secondinput, which indicates which assembly (out of
	 * the 2) the method should focus on. The third parameter is called
	 * termCode, which indicates which semester of data from the excel sheet the
	 * method should use.
	 * 
	 * @param column
	 *            number out of the 3 (0-2) that the method should focus on.
	 * @param iteration
	 *            number out of the 2 (0-1) that the method should focus on.
	 * @param semester
	 *            of data that the method should focus on (either "S1" or "S2")
	 * @return the ArrayList called shapesToCreate, which will have every
	 *         student seated with important markers.
	 */
	public static ArrayList<Integer> shapesToCreate(int firstinput,
			int secondinput, String termCode, int classToPrioritize)
					throws FileNotFoundException {
		ArrayList<Period2> period2 = MyFileIO.getList();
		int count = 0; // number of times it goes though algorithm (2)
		int j = 0; // column number
		int i = 0; // row number
		int numRowSeats = seats[i][j]; // number of seats in row
		int numColSeats = totals[j]; // number of seats in column
		int flag = 0;
		int anotherFlag = 0;
		boolean done;
		ArrayList<Integer> shapesToCreate = new ArrayList<Integer>();
		// main loop; iterates through the data piece by piece
		flower: for (int k = 0; k < period2.size(); k++) {
			// checks for inputed semester
			if (period2.get(k).getTermCode().equals(termCode)) {
				int numStudents;

				if (flag == 0 && classToPrioritize > -1) {
					numStudents = period2.get(classToPrioritize)
							.getTotalStudents(); // number of students in the
					// class
					k--;
					flag++;
				} else {
					if (k == classToPrioritize)
						k++;
					numStudents = period2.get(k).getTotalStudents();
					flag++;
				}
				if (numStudents > 3) // add a teacher if class size greater than
					// 3
					numStudents++;

				if (numStudents > numColSeats && j == firstinput
						&& count == secondinput) { // checks if the class will
					// fit in the column
					for (int z = 0; z < numColSeats; z++) { // designate empty
						// seats by adding
						// 500 for each one
						numRowSeats--;
						shapesToCreate.add(500);
						if (numRowSeats <= 0) {
							shapesToCreate.add(0);
							if (i < 21) {
								numRowSeats = seats[i + 1][j];
							}
						}
					}
					numColSeats = 0;
				}
				while (numStudents > 0) { // while class still has students
					// left, seat them
					done = false;
					for (; numRowSeats > 0 && numStudents > 0
							&& numColSeats > 0; numRowSeats--) {
						// if(j==firstinput && count==secondinput &&
						// firstinput==1){
						// if(numRowSeats==14 && done==false){
						// System.out.print(" FOURTEEN ");
						// done=true;
						// }
						// if(numRowSeats==13 && done==false){
						// shapesToCreate.add(501);
						// System.out.print(" THIRTEEN ");
						// done=true;
						// }
						// if(numRowSeats==12 && done==false){
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// System.out.print(" TWELVE ");
						// done=true;
						// }
						// if(numRowSeats==11 && done==false){
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// System.out.print(" ELEVEN ");
						// done=true;
						// }
						// if(numRowSeats==10 && done==false){
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// System.out.print(" TEN ");
						// done=true;
						// }
						// if(numRowSeats==9 && done==false){
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// shapesToCreate.add(501);
						// System.out.print(" NINE ");
						// done=true;
						// }
						// }
						if (j == firstinput && count == secondinput) {
							if (classToPrioritize > -1
									&& anotherFlag <= period2.get(
											classToPrioritize)
											.getTotalStudents()) {
								shapesToCreate.add(period2.get(
										classToPrioritize).getRoomNumber());
								anotherFlag++;
							} else {
								shapesToCreate.add(period2.get(k)
										.getRoomNumber());
							}
						}
						numColSeats--;
						numStudents--;
					}
					if (numRowSeats == 0 || numColSeats == 0) { // if a row or
						// column runs
						// out of seats,
						// move on to
						// the next row
						// or column
						shapesToCreate.add(0);
						i++;
						if (i < 22) {
							numRowSeats = seats[i][j];
						} else {
							i = 0;
							j++;
							if (j == 3) {
								j = 0;
								count++;
							}
							if (count == 2) { // if all 6 columns have been
								// done, exit the main loop
								break flower;
							}
							numRowSeats = seats[i][j];
							numColSeats = totals[j];
						}
					}
				}
			}
		}
		while (shapesToCreate.get(0) == 0) { // removes leading zeros from the
			// arraylist to be returned
			shapesToCreate.remove(0);
		}
		return shapesToCreate; // returns an arraylist with every class' room
		// number listed for the amount of students
		// in that class, with zeros marking the end of each row and 500's
		// marking empty seats
		// (considering all three inputs given to the method)
	}

}
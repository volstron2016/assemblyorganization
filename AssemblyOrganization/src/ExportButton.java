import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Accessed from seatingTester button "exportButton" which will generate a txt of the masterPanel
 */

public class ExportButton implements ActionListener {

	JButton button;
	JPanel seats;
	JFrame frame;
	JFileChooser chooser;
	public ExportButton(JButton b, JFrame frame){
		
		button = b;
		//this.seats = maps;
		this.frame = frame;
	}
	
	public void actionPerformed(ActionEvent e) {
	    //Handle open button action.
	   
		chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle("Save...");
	    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
	    //
	    // disable the "All files" option.
	    //
	    chooser.setAcceptAllFileFilterUsed(false);
	    // 
	    String str = "";
	    if (chooser.showOpenDialog(button) == JFileChooser.APPROVE_OPTION) { 
	     str  = chooser.getSelectedFile().toString();
	     System.out.println(str);
	    } else {
	      System.out.println("No Selection ");
	      }
	    
	     
	     BufferedImage bi = new BufferedImage(frame.getSize().width, frame.getSize().height, BufferedImage.TYPE_INT_ARGB); 
	     Graphics g = bi.createGraphics();
	    
//	    BufferedImage bimage = new BufferedImage(exported.getSize().width, exported.getSize().height, BufferedImage.TYPE_INT_ARGB);
//	    Graphics g = bimage.createGraphics();

	     
	     frame.paint(g);  //this == JComponent
	     g.dispose();
	     try{ImageIO.write(bi,"png",new File(str+"\\auditorium_seating_chart.png"));}catch (Exception e1) {}
		       
		   
		}
	}



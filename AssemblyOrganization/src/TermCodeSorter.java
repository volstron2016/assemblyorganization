import java.util.Comparator;

/**
 * Sorts the data for MyFileIO so that its in numerical order by TermCode and RoomNumber
 * 
 *
 */

public class TermCodeSorter implements Comparator<Period2> {
	
	
	
	@Override
	public int compare(Period2 arg0, Period2 arg1) {
		if(((Period2)arg0).TermCode.compareTo(((Period2)arg1).TermCode) == 0){
			return ((Period2)arg0).RoomNumber - (((Period2)arg1).RoomNumber);
		}	
		else{
			return ((Period2)arg0).TermCode.compareTo(((Period2)arg1).TermCode);
		}
	}
	

}
